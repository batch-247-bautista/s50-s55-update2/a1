import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';


export default function Register() {

	const {user} = useContext(UserContext);

	// State hooks to store values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNumber, setMobileNumber] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// State to store success/error message from API response
	const [message, setMessage] = useState("");

	function registerUser(e){

		e.preventDefault();

		// Send POST request to API endpoint
		fetch('/api/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName,
				lastName,
				mobileNumber,
				email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			if (data.success) {
				// Registration successful, display success message and redirect to login page
				setMessage("Thank you for registering!");
				setTimeout(() => {
					window.location.href = '/login';
				}, 4004);
			} else {
				// Registration failed, display error message
				setMessage(data.message);
			}
		})
		.catch(error => {
			// Error in API request, display error message
			setMessage("An error occurred while registering. Please try again later.");
			console.error(error);
		});

		// Clear input fields
		setFirstName("");
		setLastName("");
		setMobileNumber("");
		setEmail("");
		setPassword1("");
		setPassword2("");
	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match

		if((firstName !== "" && lastName !== "" && mobileNumber !== "" && email !== "" && password1 !== "" && password2 !== "") 
			&& (password1 === password2) 
			&& (password1.length >= 8) 
			&& (mobileNumber.length >= 11)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2]);

	return (
		(user.id !== null) ? 
		<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter first name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
				type="text"
				placeholder="Enter last name"
				value={lastName}
				onChange={e => setLastName(e.target.value)}
				required
				/>
			</Form.Group>
					<Form.Group controlId="mobileNumber">
			<Form.Label>Mobile Number</Form.Label>
			<Form.Control
				type="tel"
				placeholder="Enter mobile number"
				value={mobileNumber}
				onChange={e => setMobileNumber(e.target.value)}
				required
			/>
		</Form.Group>

		<Form.Group controlId="email">
			<Form.Label>Email address</Form.Label>
			<Form.Control
				type="email"
				placeholder="Enter email"
				value={email}
				onChange={e => setEmail(e.target.value)}
				required
			/>
		</Form.Group>

		<Form.Group controlId="password1">
			<Form.Label>Password</Form.Label>
			<Form.Control
				type="password"
				placeholder="Password"
				value={password1}
				onChange={e => setPassword1(e.target.value)}
				required
			/>
		</Form.Group>

		<Form.Group controlId="password2">
			<Form.Label>Confirm Password</Form.Label>
			<Form.Control
				type="password"
				placeholder="Verify password"
				value={password2}
				onChange={e => setPassword2(e.target.value)}
				required
			/>
		</Form.Group>

		<Button variant="primary" type="submit" disabled={!isActive}>
			Submit
		</Button>

		{message !== "" && <div>{message}</div>}
	</Form>
)
}